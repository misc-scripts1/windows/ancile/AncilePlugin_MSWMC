# Ancile Microsoft Windows Media Center Plugin
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSWMC

Ancile Microsoft Windows Media Center Plugin disables telemetry for Microsoft's Windows Media Center.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile